 <?php
require_once 'DB/connect_db.php';

if(!empty($_GET['id'])){
    $id = $_GET['id'];
    $sql = "SELECT * FROM shop_products WHERE id=".$id;
    $result = $pdoDB->query($sql);
    $result_obj = $result->fetchObject();
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Document</title>
</head>
<body>
<h2>Edit Product:</h2>

<form action="product_update.php" method="post">
    <label for="title">Title product:</label><br>
    <textarea name="title" id="title"><?=$result_obj->title?></textarea><br>
    <textarea name="price" id="price"><?=$result_obj->price?></textarea><br>
    <textarea name="description" id="description"><?=$result_obj->description?></textarea><br>
    <textarea name="type" id="type"><?=$result_obj->type?></textarea><br>
    <input type="hidden" name="title_id" value="<?=$result_obj->id?>">
    <button type="submit">Update</button>
</form>

</body>
</html>