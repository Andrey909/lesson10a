<?php
header('Content-Type: text/html; charset=utf-8');
require_once 'connect_db.php';

try{
	$sqlQuery = '
		CREATE TABLE shop_products(
			id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
			title VARCHAR(255),
			price Float(24),
			description TEXT,
			type VARCHAR(255))
	';
	$pdoDB->exec($sqlQuery);
}catch(PDOException $e){
	die('Не удалось создать таблицу shop_products!<br>'.$e->getMessage());
}